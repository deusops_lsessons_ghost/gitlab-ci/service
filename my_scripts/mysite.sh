#!/bin/sh

# $1 - branch name

cat << EOF > temp/mysite/config.toml 
baseURL = ""
languageCode = "en-us"
title = "Ghost's test site"
EOF

cat << HTML > temp/mysite/layouts/index.html
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ .Site.Title }}</title>
  </head>
  <body>

    <center>
      <header>
        <h1>{{ .Site.Title }}</h1>
      </header>

      <main>
          <h2>$1 branch</h2>
          Some other text, it shows that everything is working fine!
      </main>

      <footer>
        <p><small>Made with gitlab-runner and Hugo</small></p>
      </footer>
    </center>

  </body>
</html>
HTML
